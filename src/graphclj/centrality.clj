(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [graphclj.tools :as tools]
              [clojure.set :as set]))


(defn degrees [g]
  "Calculates the degree centrality for each node"
  (loop [g g
         res g]
    (if (seq g)
      (recur (rest g) 
             (assoc res (ffirst g) 
                    (assoc (second (first g)) :degree 
                           (let [ens (get (second (first g)) :neigh)]
                             (count ens)))))
      res)))

(defn ens-graph [g n]
  "Returns the set of the neighbors of node n"
  (get (get g n) :neigh))


(defn union-vec [v1 v2]
  "Union between 2 vectors"
  (loop [v2 v2
         vres v1]
    (if (seq v2)
      (if (contains? (set vres) (first v2))
        (recur (rest v2) vres)
        (recur (rest v2) (conj vres (first v2))))
      vres)))

(defn distance-n [g n n2]
  "Short path between two nodes"
  (loop [node [n]
           neigh []
           visit #{}
           dist 0.0]
      
      (if (seq node)
        (if (contains? visit (first node))
          (recur (rest node) neigh visit dist)
          (if (= (first node) n2)
            dist
            (recur (rest node) 
                   (union-vec neigh (vec (ens-graph g (first node))))
                   (conj visit (first node))
                   dist)))
        (if (seq neigh)
          (recur neigh [] visit (inc dist))
          (throw (Throwable. (str "Pas de chemin depuis le noeud " n ";" n2)))))))

(defn distance [g n]
  "Calculate the distances of one node to all the others"
  (loop [g2 g
         m {}]
    (if (seq g2)
      (if (not= (ffirst g2) n)
        (recur (rest g2) (assoc m (ffirst g2) (distance-n g n (ffirst g2))))
        (recur (rest g2) (assoc m (ffirst g2) 0.0)))
      m)))

(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  (loop [cent 0.0
         dis (distance g n)]
    (if (seq dis)
       (recur (+ cent (if (zero? (second (first dis)))
                       0.0
                       (/ 1 (second (first dis))))) (rest dis))
      cent)))


(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  (loop [i (ffirst g)
         g g
         res g]
    (if (seq g)
      (recur (ffirst (rest g)) (rest g) (assoc res i (assoc (get res i) :close (closeness res i))))
      res)))
